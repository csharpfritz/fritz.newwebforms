﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.Mobile.aspx.cs" Inherits="Fritz.NewWebForms.Default_Mobile" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>ASP.NET 4.5.1 FTW!</title>
  <meta name="viewport" content="width=device-width" />

  <link href="Content/bootstrap.min.css" rel="stylesheet" />
  <link href="Content/Site.css" rel="stylesheet" />
  <%--
      <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>

    <webopt:bundlereference runat="server" path="~/Content/css" />
  --%>
  <script src="Scripts/modernizr-2.6.2.js"></script>
  <script src="Scripts/bootstrap.min.js"></script>
  <script src="Scripts/jquery-1.10.2.min.js"></script>
  <script src="Scripts/respond.min.js"></script>

</head>
<body>
  <form id="form1" runat="server">

    <telerik:RadScriptManager runat="server"></telerik:RadScriptManager>

    <div class="row">
      <header class="col-xs-12">
        <h1>My Game Shop</h1>
      </header>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <asp:GridView Width="100%" runat="server" ID="grid" AutoGenerateColumns="False" DataKeyNames="Id"
          SelectMethod="grid_GetData"
          UpdateMethod="grid_UpdateItem"
          DeleteMethod="grid_DeleteItem"
          CssClass="table-responsive table-striped" >
          <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" ReadOnly="true"></asp:BoundField>
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
            <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" DataFormatString="{0:$0.00}" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
            <asp:BoundField DataField="NumInStock" HeaderText="# in Stock" SortExpression="NumInStock"></asp:BoundField>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
          </Columns>
        </asp:GridView>
      </div>
    </div>

    <div class="row">
      <asp:FormView runat="server" ID="addForm" DefaultMode="Insert" InsertMethod="addForm_InsertItem">
        <InsertItemTemplate>
          <div class="col-sm-4">
            Name:
              <asp:TextBox runat="server" ID="name" Text='<%#: Bind("Name") %>' />
          </div>
          <div class="col-sm-3">
            Price:<br />
            <asp:TextBox runat="server" ID="price" Text='<%#: Bind("Price") %>' Width="80px" />
          </div>
          <div class="col-sm-3">
            # in Stock:<br />
            <asp:TextBox runat="server" ID="numInStock" Text='<%#: Bind("NumInStock") %>' Width="80px" />
          </div>
          <div class="col-sm-2">
            <asp:Button runat="server" ID="addButton" Text="Add" CommandName="Insert" />
          </div>
        </InsertItemTemplate>
      </asp:FormView>
    </div>

    <div class="row">
      <div class="col-sm-6">
        <telerik:RadHtmlChart runat="server" ID="stockPie"
          SelectMethod="grid_GetData">
          <PlotArea>
            <Series>
              <telerik:PieSeries DataFieldY="NumInStock" NameField="Name"></telerik:PieSeries>
            </Series>
          </PlotArea>
        </telerik:RadHtmlChart>

      </div>
      <div class="col-sm-6">
        <telerik:RadHtmlChart runat="server" ID="incomePie"
          SelectMethod="grid_GetData">
          <PlotArea>
            <Series>
              <telerik:PieSeries DataFieldY="Price" NameField="Name"></telerik:PieSeries>
            </Series>
          </PlotArea>
        </telerik:RadHtmlChart>
      </div>
    </div>

  </form>
</body>
</html>
