using System;
using System.Collections.Generic;
using System.Linq;

namespace Fritz.NewWebForms.Models
{
  public class BoardGameRepository
  {

    private static readonly List<BoardGame> _Games = new List<BoardGame>
    {
      new BoardGame()
      {
        Id = 1,
        Name = "Checkers",
        Price = 7.99M,
        NumInStock = 10
      },
      new BoardGame()
      {
        Id = 2,
        Name = "Chess",
        Price = 10.99M,
        NumInStock = 20
      },
      new BoardGame()
      {
        Id = 3,
        Name = "Backgammon",
        Price = 13.99M,
        NumInStock = 25
      },
      new BoardGame()
      {
        Id = 4,
        Name = "Parcheesi",
        Price = 8.99M,
        NumInStock = 7
      }
    };

    public BoardGame GetById(int id)
    {
      return _Games.First(b => b.Id == id);
    }

    public IEnumerable<BoardGame> Get(Func<BoardGame, bool> where)
    {
      return _Games.Where(where);
    }

    public IEnumerable<BoardGame> GetAll()
    {
      return Get(_ => true);
    }

    public void Add(BoardGame newGame)
    {
      newGame.Id = _Games.Max(b => b.Id) + 1;
      _Games.Add(newGame);
    }

    public void Update(BoardGame update)
    {
      Delete(update.Id);
      _Games.Add(update);
    }

    public void Delete(int id)
    {
      _Games.Remove(GetById(id));
    }

    public void Delete(BoardGame game)
    {
      Delete(game.Id);
    }

  }

}