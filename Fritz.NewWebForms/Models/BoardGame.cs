﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fritz.NewWebForms.Models
{
  public class BoardGame
  {

    public int Id { get; set; }

    public string Name { get; set; }

    public decimal Price { get; set; }

    public int NumInStock { get; set; }

  }

}