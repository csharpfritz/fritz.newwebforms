﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Fritz.NewWebForms
{
  public partial class Default : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    // The return type can be changed to IEnumerable, however to support
    // paging and sorting, the following parameters must be added:
    //     int maximumRows
    //     int startRowIndex
    //     out int totalRowCount
    //     string sortByExpression
    public IQueryable grid_GetData()
    {
      var repo = new Models.BoardGameRepository();
      return repo.GetAll().AsQueryable();
    }

    // The id parameter name should match the DataKeyNames value set on the control
    public void grid_UpdateItem(int id, Models.BoardGame game)
    {
      var repo = new Models.BoardGameRepository();
      repo.Update(game);
    }

    // The id parameter name should match the DataKeyNames value set on the control
    public void grid_DeleteItem(int id)
    {
      var repo = new Models.BoardGameRepository();
      repo.Delete(id);
    }

    public void addForm_InsertItem(Models.BoardGame newGame)
    {
      var repo = new Models.BoardGameRepository();
      repo.Add(newGame);
      Response.Redirect("Default.aspx");
    }
  
  }

}