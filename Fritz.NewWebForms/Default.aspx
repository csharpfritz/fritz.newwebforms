﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Fritz.NewWebForms.Default" EnableViewState="true" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ASP.NET 2.0 Rocks!</title>
</head>
<body>
    <form id="form1" runat="server">

      <telerik:RadScriptManager runat="server"></telerik:RadScriptManager>

    <div>
    
      <h1>My Game Shop</h1>

      <h2>Current Prices</h2>
      <asp:GridView Width="100%" runat="server" ID="grid" AutoGenerateColumns="False" DataKeyNames="Id"
        DataSourceId="ds"
        >
        <Columns>
          <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" ReadOnly="true"></asp:BoundField>
          <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
          <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" DataFormatString="{0:$0.00}" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
          <asp:BoundField DataField="NumInStock" HeaderText="NumInStock" SortExpression="NumInStock"></asp:BoundField>
          <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"></asp:CommandField>
        </Columns>
      </asp:GridView>

      <table width="100%">
        <tr>
          <td colspan="3">
            <asp:FormView runat="server" id="addForm" DefaultMode="Insert" InsertMethod="addForm_InsertItem">
              <InsertItemTemplate>
                Name:
                <asp:TextBox runat="server" ID="name" Text='<%#: Bind("Name") %>'></asp:TextBox>
                Price:
                <asp:TextBox runat="server" ID="price" Width="80px" Text='<%#: Bind("Price") %>' ></asp:TextBox>
                Num in Stock:
                <asp:TextBox runat="server" ID="numInStock" Width="50px" Text='<%#: Bind("NumInStock") %>' ></asp:TextBox>
                <asp:Button runat="server" ID="add" Text="Add" CommandName="Insert" />
              </InsertItemTemplate>
            </asp:FormView>
          </td>
        </tr>
        <tr>
          <td width="50%">

            <telerik:RadHtmlChart runat="server" ID="stockPie" 
              DataSourceID="ds">
              <PlotArea>
                <Series>
                  <telerik:PieSeries DataFieldY="NumInStock" NameField="Name"></telerik:PieSeries>
                </Series>
              </PlotArea>
            </telerik:RadHtmlChart>

          </td>
          <td width="50%">

            <telerik:RadHtmlChart runat="server" ID="incomePie"
              DataSourceID="ds">
              <PlotArea>
                <Series>
                  <telerik:PieSeries DataFieldY="Price" NameField="Name"></telerik:PieSeries>
                </Series>
              </PlotArea>
            </telerik:RadHtmlChart>

          </td>

        </tr>
      </table>

      <asp:ObjectDataSource runat="server" ID="ds"
        TypeName="Fritz.NewWebForms.Models.BoardGameRepository"
        DataObjectTypeName="Fritz.NewWebForms.Models.BoardGame"
        SelectMethod="GetAll"
        UpdateMethod="Update" 
        DeleteMethod="Delete"
        InsertMethod="Add">

        <DeleteParameters>
          <asp:Parameter Name="id" Type="Int32"></asp:Parameter>
        </DeleteParameters>
      </asp:ObjectDataSource>

    </div>
    </form>
</body>
</html>
